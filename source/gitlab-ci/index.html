---
layout: default
title: GitLab Continuous Integration
suppress_header: true
extra_css:
  - gitlab-ci.css
extra_js:
  - gitlab-ci.js
---

<div class="wrapper">
  <div class="image-title">
    <div class="color-bg">
      <div>
        <h1>GitLab Continuous Integration</h1>
        <h3 id="ci-subt">Let GitLab CI test, build, deploy your code</h3>
      </div>
      <div>
        <a href="https://about.gitlab.com/downloads/" id="download">Download GitLab</a>
        <a href="https://ci.gitlab.com/user_sessions/new" id="signin">Sign in to ci.gitlab.com</a>
      </div>
    </div>
  </div>

<div class="sub-wrapper">
  <div class="container">
    <div class="row advantages">
      <div class="col-xs-1 col-md-1"></div>
      <div class="col-xs-10 col-md-10">
        <h1>What Are The Advantages?</h1>
        <ul>
          <li><b>Easy to set up</b> since it is included in Omnibus packages of GitLab or use it for free on <a href="https://ci.gitlab.com/">ci.gitlab.com</a></li>
          <li><b>Beautiful interface</b> with a clear menu structure</li>
          <li><b>Performant and stable</b>, as tests run distributed on separate machines</li>
          <li><b>Receive test results faster</b> with each commit running in parallel on multiple jobs</li>
          <li><b>Free to use and completely open source</b> all CI code is MIT licensed</li>
        </ul>
      </div>
      <div class="col-xs-1 col-md-1"></div>
    </div>

    <hr class="divider" />

    <div class="row more-features">
      <div class="col-xs-1 col-md-1"></div>
      <div class="col-md-5 hidden-xs hidden-sm">
        <div class="gitlab-ci"></div>
      </div>
      <div class="col-xs-10 col-md-5">
        <h1>Features</h1>
        <ul>
          <li><b>Single sign-on</b>: Use the same login and password as on your GitLab instance.</li>
          <li><b>Quick project setup</b>: Add your project in a single click, all hooks are setup automatically via the GitLab API.</li>
          <li><b>Elegant and flexible</b>: Build scripts are written in bash, test projects in any programming language.</li>
          <li><b>Merge request integration</b>: See the status of the test within the Merge Request in GitLab.</li>
          <li><b>Distributed by default</b>: GitLab CI and build runners can run on separate machines providing more stability.</li>
          <li><b>Realtime logging</b>: A link in the merge request takes you to the current build log that updates dynamically.</li>
          <li><b>Parallel builds</b>: Split a build over multiple runners so it executes quickly.</li>
        </ul>
      </div>
      <div class="col-xs-1 col-md-1"></div>
    </div>

    <hr class="divider" />

    <div class="row integration">
      <div class="col-xs-1 col-md-1"></div>
      <div class="col-xs-10 col-md-6">
        <h1>Fully integrated with GitLab</h1>
        <ul>
          <li>GitLab CI is fully integrated with GitLab.</li>
          <li>No need for separate authentication and all your projects are immediately available.</li>
        </ul>
      </div>
      <div class="hidden-xs hidden-sm col-md-4">
        <img src="/images/ci/gitlab-raccoon-dog.png" class="gitlab-raccoon-dog-image hidden-xs" alt="" />
      </div>
      <div class="col-xs-1 col-md-1"></div>
    </div>

    <hr class="divider" />

    <div class="row architecture">
      <div class="col-xs-1 col-md-1"></div>
      <div class="col-xs-10 col-md-5">
        <h1>Architecture</h1>
        <div class="architecture-text">
          <p><b>GitLab CI</b> is a web application with an API that stores its state in a database. It manages projects/builds and provides a nice user interface. It uses the GitLab application API to authenticate its users.</p>
          <p>GitLab Runner is an application which processes builds. It can be deployed separately and works with GitLab CI through an API.</p>
          <p>In order to run tests, you need at least 1 <b>GitLab CI</b> instance and 1 <b>GitLab Runner</b>. However, for running several builds at the same time, you may want to set up more than one <b>GitLab Runner</b>.
          Multiple runners will also allow you to run the tests for one commit in parallel so it finishes sooner.
          Runners can be installed on the same machine as GitLab CI or on other machines; we recommend to install them on other machines.
          </p>
        </div>
      </div>
      <div class="hidden-xs hidden-sm col-md-6">
          <img src="/images/ci/arch-1.jpg" class="gitlab-arch hidden-xs" alt="" />
      </div>
    </div>

    <hr class="divider" id="gitlab-runner"  />

    <div class="row gitlab-runner">
      <div class="col-xs-1 col-md-1"></div>
      <div class="col-xs-10 col-md-10">
        <h1>GitLab Runner</h1>
        <p>To perform the actual build, you need to install GitLab Runner.</p>
        <ul>
          <li><a href="https://gitlab.com/gitlab-org/omnibus-gitlab-runner/blob/master/doc/install/README.md"><b>GitLab Runner Omnibus package for Linux</b></a> (the recommended way to install GitLab Runner)</li>
          <li><a href="https://gitlab.com/gitlab-org/gitlab-ci-runner">GitLab Runner source code for Linux</a></li>
          <li><a href="https://github.com/ayufan/gitlab-ci-multi-runner">Unofficial GitLab Runner in Go with Docker support for Linux, Darwin and Windows</a></li>
          <li><a href="https://github.com/virtualmarc/gitlab-ci-runner-win">Unofficial GitLab Runner for Windows</a></li>
          <li><a href="https://github.com/nafg/gitlab-ci-runner-scala">Unofficial GitLab Runner for Scala/Java</a></li>
          <li><a href="https://www.npmjs.com/package/gcr">Unofficial GitLab Runner for Node</a></li>
        </ul>
      <div class="col-xs-1 col-md-2"></div>
      </div>
    </div>

    <hr class="divider" />

    <div class="row requirements">
      <div class="col-xs-1 col-md-1"></div>
      <div class="col-md-3 hidden-xs hidden-sm">
        <i class="fa fa-linux"></i>
      </div>
      <div class="col-xs-10 col-md-7">
        <h1>Requirements</h1>
        <h4>Tests can be run on many different platforms</h4>
        <p>
          Tests can be run on Unix, Windows, OSX, the JVM and more.
          The tests are performed with GitLab Runner, see that section for all the options.
        </p>
        <h4>GitLab CI itself is officially supported on</h4>
        <ul>
          <li>Ubuntu</li>
          <li>Debian</li>
          <li>CentOS</li>
          <li>Red Hat Enterprise Linux</li>
          <li>Scientific Linux</li>
          <li>Oracle Linux</li>
        </ul>
        <h4>Hardware Requirements</h4>
        <ul>
          <li>1GB of memory or more is recommended, 512MB works</li>
          <li>2 CPU cores or more are recommended, 1 CPU core works</li>
          <li>100MB of disk space</li>
        </ul>
      </div>
      <div class="col-xs-6 col-md-1"></div>
    </div>

    <hr class="divider" />

    <div class="row installation">
      <div class="col-xs-1 col-md-1"></div>
      <div class="col-md-3 hidden-xs hidden-sm">
        <i class="fa fa-download 5x"></i>
      </div>
      <div class="col-xs-10 col-md-7">
        <h1>Installation</h1>
        <ul>
          <li><a href="https://about.gitlab.com/downloads/"><b>Omnibus packages</b></a> (recommended way to install) include the CI coordinator, see the <a href="https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/gitlab-ci/README.md">configuration instructions</a></li>
          <li><a href="https://gitlab.com/gitlab-org/gitlab-ci/blob/master/doc/install/installation.md">Manual installation guide</a></li>
          <li><a href="https://github.com/sameersbn/docker-gitlab-ci">Unofficial Docker Image by Sameer Naik</a></li>
          <li><a href="https://registry.hub.docker.com/u/anapsix/gitlab-ci/">Unofficial Docker Image by Anastas Dancha</a>
          <li><a href="https://gitlab.com/gitlab-org/gitlab-development-kit">GitLab Development Kit</a> is recommended for development work</li>
          <li>Use it for free on <a href="https://ci.gitlab.com/">ci.gitlab.com</a> if you have a GitLab.com account (you do need to bring your own runner)</li>
        </ul>
      </div>
      <div class="col-xs-1 col-md-1"></div>
    </div>

    <hr class="divider" />

    <div class="row links">
      <div class="col-xs-1 col-md-1"></div>
      <div class="col-xs-11 col-md-6">
        <h1>Help and More Information</h1>
        <ul>
          <li>Please see <a href="https://about.gitlab.com/getting-help/">Getting help for GitLab</a> if you have questions.</li>
          <li>Propose and discuss new features of CI in the <a href="http://feedback.gitlab.com/forums/176466-general/category/64310-gitlab-ci">feature request forum.</a></li>
          <li>The <a href="http://doc.gitlab.com/ci/">documentation</a> can be found on the doc site.</li>
          <li>After GitLab CI 5.4 we released 7.8 so that the versions of GitLab and GitLab CI are in sync.</li>
        </ul>
      </div>
      <div class="col-xs-6 col-md-4">
        <i class="fa fa-code 5x hidden-xs hidden-sm"></i>
      </div>
    </div>
  </div>
</div>
